from django.http import HttpResponse
from django.shortcuts import render, redirect


def home(request):

    if request.method == "GET":
        return HttpResponse(request.META['REMOTE_ADDR'])
    else:
        return HttpResponse("This URL can only be accessed via GET method")
