from django.test import TestCase


class AppTestCase(TestCase):
    def test_get_homepage(self):

        response = self.client.get("/")
        self.assertEqual("127.0.0.1", response.content.decode())
    
    def test_beside_get_homepage(self):

        response = self.client.post("/")
        self.assertEqual("This URL can only be accessed via GET method", response.content.decode())
