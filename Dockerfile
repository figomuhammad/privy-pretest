FROM python:3.9-alpine

ENV PYTHONBUFFERED 1
ENV PYTHONWRITEBYTECODE 1

ENV APP_HOME=/home/app/web

# Change the workdir.
WORKDIR $APP_HOME

COPY requirements.txt $APP_HOME
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

COPY . $APP_HOME

EXPOSE 8000

WORKDIR $APP_HOME/simple-web-app/simplewebapp
CMD gunicorn simplewebapp.wsgi:application --bind 0.0.0.0:8000
